# README #



### What is this repository for? ###

The goal of this bundle is to be notified from external "continuous testing" frameworks about their test results.
Once notified, we parse these results into an unified format, and notify back some clients services interested about tests results.

My original goal is the following:

Get Karma tests results directly into Eclipse, like Infinitest does for Java.
Karma is standalone, external, but can easily be extended with Node.js plugins.
So I want Karma to connect to a ServerSocket, get the json results parsed to a formalized java object, and notify Eclipse so it can display errors in the code, Problem view, etc...

```
+-------+                                                                       
|Karma  |                                                                       
|Test   +-+                                                                     
|Results| |                                                                     
+-------+ |                                                                     
          |                                                                     
          |                                                                     
          |                                                                     
          |                                                                     
          v                                                                     
       +             +                                                          
       |  Listening  |           Parse String                                   
       |  Server     |           ITestResultsParserService                      
       |  Socket     |                                                          
       |             |           +-----------+  - KarmaTestResultParser         
       +-----+-------+           |              - Other possible parsers...     
             |                   |              (only one must parse the string)
             |                   |                                              
             |                   |                                              
             |                   |                                              
   +---------+-------------+     |  Notify Clients                              
   |                       +-----+  ITestResultsService                         
   | TestResultsDispatcher |                                                    
   |                       +-----------------+  - EclipseTestResultsService     
   +-----------------------+                    - Other possibles clients       
                                                (all clients are notified)      
```


