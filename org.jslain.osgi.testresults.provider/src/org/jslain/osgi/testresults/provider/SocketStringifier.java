package org.jslain.osgi.testresults.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketStringifier {

	public String toString(Socket socket){
		StringBuilder sb = new StringBuilder();
		
		try(BufferedReader bis = new BufferedReader(new InputStreamReader(socket.getInputStream()))){
			String line = null;
			while((line = bis.readLine()) != null){
				sb.append(line).append(System.getProperty("line.separator"));
			}
		}catch(IOException e){
			
		}
		
		return sb.toString();
	}
}
