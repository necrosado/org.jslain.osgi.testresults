package org.jslain.osgi.testresults.provider;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jslain.osgi.testresults.api.ITestResultsParserService;
import org.jslain.osgi.testresults.api.StackElementDto;
import org.jslain.osgi.testresults.api.TestResultDto;
import org.jslain.osgi.testresults.api.TestResultsDto;
import org.osgi.service.component.annotations.Component;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

@Component
public class KarmaResultParser implements ITestResultsParserService{

	private static final Pattern VERSION_PATTERN = Pattern.compile("\\s*version\\s*=\\s*KARMA\\-1\\.0\\.0\\s*(.*)", Pattern.MULTILINE);
	private static final Pattern STACK_PATTERN = Pattern.compile(".*localhost\\:?\\d*(.*?\\.js).*?(\\d+)\\:(\\d+)\\)", Pattern.MULTILINE | Pattern.DOTALL);
	
	@Override
	public boolean canParse(String source) {
		return source != null && VERSION_PATTERN.matcher(source).matches();
	}
	
	@Override
	public TestResultsDto parse(String source) {
		TestResultsDto result = null;
		
		Matcher m = VERSION_PATTERN.matcher(source);
		if(m.find()){
			result = new TestResultsDto();
			
			source = m.group(1);
			
			System.out.println(source);
			
			JsonArray jsonArray = JsonArray.readFrom(source);
			
			for(JsonValue value : jsonArray){
				JsonObject jsonObject = value.asObject();
				
				TestResultDto toAdd = new TestResultDto();
				toAdd.setDescription(jsonObject.getString("description", "[No description found]"));
				toAdd.setId(jsonObject.getString("id", "[No id found]"));
				//toAdd.setLog(jsonObject.getString("log", "[No logs found]"));
				toAdd.setSkipped(jsonObject.getBoolean("skipped", false));
				toAdd.setSuccess(jsonObject.getBoolean("success", true));
				//toAdd.setSuite(jsonObject.getString("suite", "[No test suite found]"));
				toAdd.setTime(jsonObject.getInt("time", -1));
				
				for(JsonValue valueSuite : jsonObject.get("suite").asArray()){
					toAdd.addSuite(valueSuite.asString());
				}
				
				for(JsonValue valueStack : jsonObject.get("log").asArray()){
					StackElementDto stackToAdd = new StackElementDto();
					stackToAdd.setOriginalText(valueStack.asString());
					
					Matcher stackMatcher = STACK_PATTERN.matcher(valueStack.asString());
					
					if(stackMatcher.matches()){
						stackToAdd.setResourcePath(stackMatcher.group(1));
						stackToAdd.setLineNumber(Integer.parseInt(stackMatcher.group(2)));
						stackToAdd.setColumnNumber(Integer.parseInt(stackMatcher.group(3)));
					}
					
					toAdd.addStackElement(stackToAdd);
				}
				
				result.addResult(toAdd);
			}
		
		}
		
		return result;
	}
}
