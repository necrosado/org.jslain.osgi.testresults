package org.jslain.osgi.testresults.provider;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;

import org.jslain.osgi.socket.api.IServerSocketController;
import org.jslain.osgi.socket.api.IServerSocketFactory;
import org.jslain.osgi.socket.api.ISocketHandler;
import org.jslain.osgi.testresults.api.ITestResultsParserService;
import org.jslain.osgi.testresults.api.ITestResultsService;
import org.jslain.osgi.testresults.api.TestResultsDto;
import org.jslain.osgi.testresults.api.TestStatus;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

/**
 * 
 */
@Component(name = "org.jslain.osgi.testresults", immediate=true)
public class TestResultsDispatcher implements ISocketHandler{
	
	private IServerSocketFactory serverSockerFactory;
	private SocketStringifier socketStringifier;

	private Collection<ITestResultsParserService> testResultsParserServices;
	private Collection<ITestResultsService> testResultsServices;
	private IServerSocketController serverSocketController;
	
	@Activate
	public void activated() throws IOException{
		socketStringifier = new SocketStringifier(); 
		testResultsParserServices = new ArrayList<>();
		testResultsServices = new ArrayList<>();
		
		serverSocketController = serverSockerFactory.createServerSocket(7357, this);
		serverSocketController.start();
	}
	
	@Reference
	public void setServerSocketFactory(IServerSocketFactory serverSocketFactory){
		this.serverSockerFactory = serverSocketFactory;
	}
	public void unsetServerSocketFactory(IServerSocketFactory serverSocketFactory) throws IOException{
		this.serverSockerFactory = null;
		serverSocketController.stop();
		serverSocketController = null;
	}

	@Reference(cardinality= ReferenceCardinality.MULTIPLE,
			policy = ReferencePolicy.DYNAMIC,
			service = ITestResultsParserService.class)
	public void bindTestResultsParser(ITestResultsParserService parser) {
		testResultsParserServices.add(parser);
	}
	public void unbindTestResultsParser(ITestResultsParserService parser) {
		testResultsParserServices.remove(parser);
	}
	
	@Reference(cardinality= ReferenceCardinality.MULTIPLE,
			policy = ReferencePolicy.DYNAMIC,
			service = ITestResultsService.class)
	public void bindTestResultsService(ITestResultsService parser) {
		testResultsServices.add(parser);
	}
	public void unbindTestResultsService(ITestResultsService parser) {
		testResultsServices.remove(parser);
	}
	
	public void setSocketStringifier(SocketStringifier socketStringifier) {
		this.socketStringifier = socketStringifier;
	}
	
	/////////////////////////////////////////
	// ISocketHandler
	/////////////////////////////////////////
	@Override
	public void onConnect(Socket socket) {
		String stringified = socketStringifier.toString(socket);
		
		//Searching for the right parser
		TestResultsDto result = null;
		for(ITestResultsParserService parser : testResultsParserServices){
			if(parser.canParse(stringified)){
				if(result != null){
					throw new IllegalStateException(
							"Multiple TestResultsParserService is able to "
							+ "parse the content of this test result. Maybe "
							+ "you have 2 versions of the same plugin?");
				}
				
				result = parser.parse(stringified);
			}
		}
		
		//Calling handlers
		for(ITestResultsService service : testResultsServices){
			service.update(TestStatus.STARTED, result);
		}
	}
	
	@Override
	public void onError(Exception e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onTerminated() {
		// TODO Auto-generated method stub
		
	}

}
