package org.jslain.osgi.testresults.provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.List;

import org.jslain.osgi.testresults.api.StackElementDto;
import org.jslain.osgi.testresults.api.TestResultDto;
import org.jslain.osgi.testresults.api.TestResultsDto;
import org.junit.Before;
import org.junit.Test;


public class KarmaResultParserTest {

	private KarmaResultParser underTest;

	@Before
	public void setup(){
		underTest = new KarmaResultParser();
	}
	
	@Test
	public void givenValidStartOfFile_whenCanParse_thenReturnTrue(){
		assertTrue(underTest.canParse("version=KARMA-1.0.0"));
	}
	
	@Test
	public void givenInvalidStartOfFile_whenCanParse_thenReturnFalse(){
		assertFalse(underTest.canParse(""));
	}
	
	@Test
	public void givenNull_whenCanParse_thenReturnFalse(){
		assertFalse(underTest.canParse(null));
	}
	
	@Test
	public void givenValidInput_whenParse_thenReturnResult(){
		StringBuilder input = new StringBuilder(" version  =  KARMA-1.0.0 \r\n\r\n")
		.append("[{")
		.append("\"description\":\"a sample description\",")
		.append("\"id\":\"spec11\",")
		.append("\"log\":[\"Failed\\n    at Object.<anonymous> (http://localhost:9876/base/root/folder/test.js?417c2d9cb0dda41be3471078b49835eaf0130dc1:7:3)\"],")
		.append("\"skipped\":false,")
		.append("\"success\":true,")
		.append("\"suite\":[\"A test suite\"],")
		.append("\"time\":6")
		.append("}]");
		
		TestResultsDto result = underTest.parse(input.toString());
		
		assertNotNull(result);
		assertEquals(1, result.size());
		
		TestResultDto result1 = result.iterator().next();
		assertEquals("a sample description", result1.getDescription());
		assertEquals("spec11", result1.getId());
		
		//assertEquals("Failed\\n    at Object.<anonymous> (http://localhost:9876/base/root/folder/test.js?417c2d9cb0dda41be3471078b49835eaf0130dc1:7:3)", result1.getLog());
		assertEquals(false, result1.getSkipped());
		assertEquals(true, result1.getSuccess());
		assertEquals(6, result1.getTime());

		Collection<String> suites = result1.getSuites();
		assertEquals(1, suites.size());
		assertEquals("A test suite", suites.iterator().next());
		
		List<StackElementDto> stackElements = result1.getStack();
		assertEquals(1, stackElements.size());
		
		StackElementDto stackElement = stackElements.get(0);
		assertEquals("Failed\n    at Object.<anonymous> (http://localhost:9876/base/root/folder/test.js?417c2d9cb0dda41be3471078b49835eaf0130dc1:7:3)", stackElement.getOriginalText());
		assertEquals(7, stackElement.getLineNumber());
		assertEquals(3, stackElement.getColumnNumber());
		assertEquals("/base/root/folder/test.js", stackElement.getResourcePath());
	}
}
