package org.jslain.osgi.testresults.provider;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.net.Socket;

import org.junit.Test;


public class SocketStringifierTest {

	@Test
	public void testStringifier() throws Exception{
		Socket mockSocket = mock(Socket.class);
		String test = "test" + System.getProperty("line.separator")
				+ "test2" + System.getProperty("line.separator")
				+ "test3" + System.getProperty("line.separator");
		ByteArrayInputStream inputStream = new ByteArrayInputStream(test.getBytes());
		
		when(mockSocket.getInputStream()).thenReturn(inputStream);
		
		SocketStringifier underTest = new SocketStringifier();
		
		String result = underTest.toString(mockSocket);
		
		assertNotNull("Stringified socket shouldn't be null", result);
		assertEquals(test, result);
		
	}
}
