package org.jslain.osgi.testresults.provider;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.Socket;

import org.jslain.osgi.socket.api.IServerSocketController;
import org.jslain.osgi.socket.api.IServerSocketFactory;
import org.jslain.osgi.testresults.api.ITestResultsParserService;
import org.jslain.osgi.testresults.api.ITestResultsService;
import org.jslain.osgi.testresults.api.TestResultsDto;
import org.jslain.osgi.testresults.api.TestStatus;
import org.junit.Before;
import org.junit.Test;



public class TestResultsDispatcherTest {

	private TestResultsDispatcher underTest;
	private IServerSocketFactory mockServerSocketFactory;
	private SocketStringifier mockSocketStringifier;
	private Socket mockSocket;
	private String stringified;
	private TestResultsDto testResultsDto;
	private IServerSocketController mockSocketController;

	@Before
	public void setup() throws Exception{
		setup_underTest();
		
		setup_SocketController();
		
		setup_testResults();
		
		setup_activation();
		
		setup_socketStringifier();
	}
	
	public void setup_underTest() throws Exception{
		underTest = new TestResultsDispatcher();
		
	}
	
	public void setup_SocketController() throws Exception{
		mockServerSocketFactory = mock(IServerSocketFactory.class);
		mockSocketController = mock(IServerSocketController.class);
		
		when(mockServerSocketFactory.createServerSocket(7357, underTest))
		.thenReturn(mockSocketController);
		underTest.setServerSocketFactory(mockServerSocketFactory);
	}
	
	public void setup_socketStringifier(){
		mockSocketStringifier = mock(SocketStringifier.class);
		underTest.setSocketStringifier(mockSocketStringifier);
		
		mockSocket = mock(Socket.class);
		stringified = "a stringified socket content";
		
		//Given
		mockSocketToString(mockSocket, stringified);
		
	}
	
	public void setup_testResults(){
		testResultsDto = new TestResultsDto();		
	}
	
	public void setup_activation() throws Exception{
		underTest.activated();
		
	}
	
	@Test
	public void aSingleParser_whenReceivingValidSocketContent_servicesAreCalled(){
		
		ITestResultsParserService mockParserAccept = mockParserAccept(stringified, testResultsDto);
		
		ITestResultsService mockService1 = mockResultService();
		ITestResultsService mockService2 = mockResultService();
		
		//When
		underTest.onConnect(mockSocket);
		
		//Then
		verify(mockParserAccept).parse(stringified);
		
		verify(mockService1).update(TestStatus.STARTED, testResultsDto);
		verify(mockService2).update(TestStatus.STARTED, testResultsDto);
	}
	
	@Test
	public void multipleParsers_whenOnlyOneCanParseTheSocketContent_servicesAreCalled(){
		ITestResultsParserService mockParserAccept = mockParserAccept(stringified, testResultsDto);
		mockParserRefuse(stringified);
		
		ITestResultsService mockService1 = mockResultService();
		ITestResultsService mockService2 = mockResultService();
		
		//When
		underTest.onConnect(mockSocket);
		
		//Then
		verify(mockParserAccept).parse(stringified);
		
		verify(mockService1).update(TestStatus.STARTED, testResultsDto);
		verify(mockService2).update(TestStatus.STARTED, testResultsDto);
	}
	
	@Test(expected=IllegalStateException.class)
	public void multiplesParsers_when2ofThemCanParseTheSocketContent_anErrorIsThrown() throws Exception{
		
		mockParserAccept(stringified, testResultsDto);
		mockParserAccept(stringified, testResultsDto);
		
		//When
		underTest.onConnect(mockSocket);
				
	}

	private ITestResultsService mockResultService() {
		ITestResultsService mockService = mock(ITestResultsService.class);
		underTest.bindTestResultsService(mockService);
		return mockService;
	}

	private void mockSocketToString(Socket socket, String string){
		when(mockSocketStringifier.toString(socket)).thenReturn(string);
	}
	
	private ITestResultsParserService mockParserAccept(String stringified, TestResultsDto results) {
		ITestResultsParserService mockParser = mock(ITestResultsParserService.class);
		underTest.bindTestResultsParser(mockParser);
		when(mockParser.canParse(stringified)).thenReturn(true);
		when(mockParser.parse(stringified)).thenReturn(results);
		
		return mockParser;
	}
	
	private ITestResultsParserService mockParserRefuse(String stringified){
		ITestResultsParserService mockParser = mock(ITestResultsParserService.class);
		underTest.bindTestResultsParser(mockParser);
		when(mockParser.canParse(stringified)).thenReturn(false);
		
		return mockParser;
	}
}
