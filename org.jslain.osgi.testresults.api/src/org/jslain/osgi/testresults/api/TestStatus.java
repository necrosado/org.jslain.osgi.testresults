package org.jslain.osgi.testresults.api;

public enum TestStatus {

	STARTED,
	ENDED,
	ERROR;
}
