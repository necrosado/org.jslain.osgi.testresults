package org.jslain.osgi.testresults.api;

/**
 * Must be defined by implementors.<br/>
 * 
 * For example, could be able to parse a Karma (javascript) test results in JSON.
 * 
 * @author ghislain
 *
 */
public interface ITestResultsParserService {
	
	boolean canParse(String source);
	
	TestResultsDto parse(String source);
}
