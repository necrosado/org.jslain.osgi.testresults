package org.jslain.osgi.testresults.api;

/**
 * 
 */
public interface ITestResultsService {
	
	void update(TestStatus testStatus, TestResultsDto results);
	
}
