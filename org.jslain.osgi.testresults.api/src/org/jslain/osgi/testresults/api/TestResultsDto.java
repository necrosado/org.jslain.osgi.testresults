package org.jslain.osgi.testresults.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class TestResultsDto implements Iterable<TestResultDto>{

	private Collection<TestResultDto> results;
	
	public TestResultsDto(){
		results = new ArrayList<TestResultDto>();
	}
	
	public int size() {
		return 1;
	}

	public Iterator<TestResultDto> iterator() {
		return results.iterator();
	}
	
	public void addResult(TestResultDto testResultDto){
		results.add(testResultDto);
	}
}
