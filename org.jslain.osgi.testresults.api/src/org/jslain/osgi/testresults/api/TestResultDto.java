package org.jslain.osgi.testresults.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestResultDto {
	private String description;
	private String id;
	private List<StackElementDto> stack;
	private Boolean skipped;
	private Boolean success;
	private Set<String> suites;
	private int time;
	
	public TestResultDto() {
		stack = new ArrayList<StackElementDto>();
		suites = new HashSet<String>();
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public List<StackElementDto> getStack() {
		return stack;
	}

	public void addStackElement(StackElementDto stackElement) {
		this.stack.add(stackElement);
	}

	public Boolean getSkipped() {
		return skipped;
	}
	public void setSkipped(Boolean skipped) {
		this.skipped = skipped;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
	public Set<String> getSuites() {
		return suites;
	}

	public void addSuite(String suite) {
		this.suites.add(suite);
	}

	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	
	
}
