package org.jslain.osgi.testresults.test;

import java.util.Hashtable;

import org.jslain.osgi.testresults.api.ITestResultsParserService;
import org.jslain.osgi.testresults.api.TestResultsDto;
import org.junit.Assert;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * 
 * 
 * 
 * 
 * 
 * 
 */

public class TestResultsIntegrationTest {

    private final BundleContext context = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
    
    @Test
    public void testTestresults() throws Exception {
    	Assert.assertNotNull(context);
    	
    	MyTestResultParser parser = new MyTestResultParser();
    	context.registerService(ITestResultsParserService.class, parser, new Hashtable<String, Object>());
    }
    
    
    <T> T getService(Class<T> clazz) throws InterruptedException {
    	ServiceTracker<T,T> st = new ServiceTracker<>(context, clazz, null);
    	st.open();
    	return st.waitForService(1000);
    }
    
    public static class MyTestResultParser implements ITestResultsParserService{
    	@Override
    	public boolean canParse(String source) {
    		return true;
    	}
    	
    	@Override
    	public TestResultsDto parse(String source) {
    		return new TestResultsDto();
    	}
    }
}
